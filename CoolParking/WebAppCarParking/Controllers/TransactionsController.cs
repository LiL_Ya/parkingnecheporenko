﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace WebAppCarParking.Controllers
{
    [Produces("application/json")]
    [Route("api/transactions")]
    public class TransactionsController : Controller
    {
        // GET: api/Transactions
        [HttpGet]
        public ActionResult GetHelp()
        {
            return Content(Helper.GetHelpTransactions());
        }

        //// GET: api/Transactions/log
        //[HttpGet("log")]
        //public ActionResult GetLog()
        //{
        //    var log = Parking.Instance.GetLastParkingTransactions();


        //    JObject json = new JObject();
        //    JArray array = new JArray();

        //    foreach (var item in log)
        //    {
        //        array.Add(new JObject(
        //            new JProperty(nameof(item.DateTime), new JValue(item.DateTime)),
        //            new JProperty(nameof(item._debited), new JValue(item.Debited))));
        //    }

        //    json.Add("Transactions", array);

        //    return Content(json.ToString());
        //}

        //// GET: api/Transactions/history
        //[HttpGet("history")]
        //public ActionResult GetHistory()
        //{
        //    JObject json = new JObject();
        //    JArray array = new JArray();

        //    foreach(var item in Parking.Instance
        //        .GetLastParkingTransactions()
        //        .Select(t => Helper.TransactionToJObject(t)))
        //    {
        //        array.Add(item);
        //    }

        //    json.Add("Transactions", array);

        //    return Content(json.ToString());
        //}


        // GET: api/Transactions/history/5
        //[HttpGet("history/{id}")]
        //public ActionResult GetHistory(string id)
        //{
        //    Vehicle car = Parking.Instance();

        //    if (car == null)
        //        return BadRequest(Helper.ErrorCarIdNotFound);

        //    JObject json = new JObject();
        //    JArray array = new JArray();

        //    foreach (var item in Parking.Instance
        //        .GetTransactionHistoryForLastMinute(car)
        //        .Select(t => Helper.TransactionToJObject(t)))
        //    {
        //        array.Add(item);
        //    }

        //    json.Add("Car", Helper.CarToJObject(car, id));
        //    json.Add("Transactions", array);

        //    return Content(json.ToString());
        //}

        // PUT: api/Transactions/5;50
        //[HttpPut("recharge/{id};{amount}")]
        //public ActionResult Put(string id, decimal amount)
        //{
        //    if (!Parking.Instance.HasCars)
        //        return BadRequest(Helper.ErrorParkingEmpty);

        //    Vehicle car = Parking.Equals(id);

        //    if (car == null)
        //        return BadRequest(Helper.ErrorCarIdNotFound);

        //    if (amount >= Vehicle.MinAmount && amount <= Vehicle.MaxAmount)
        //    {
        //        Parking.Instance.TopUpVehicle(car, amount);
        //        return Content(Helper.CarToJObject(car, id).ToString());
        //    }
        //    else
        //    {
        //        return BadRequest(Helper.ErrorCarAmountOutOfRange);
        //    }
        //}
    }
}

