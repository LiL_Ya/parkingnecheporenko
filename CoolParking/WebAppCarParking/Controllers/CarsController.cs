﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CoolParking;
using CoolParking.BL.Models;

namespace WebAppCarParking.Controllers
{
    [Produces("application/json")]
    [Route("api/cars")]
    public class CarsController : Controller
    {
        [HttpGet]
        public ActionResult GetHelp()
        {
            return Content(Helper.GetHelpCars());
        }

        // GET: api/Cars
        [HttpGet("all")]
        public ActionResult Get()
        {
            

            JObject json = new JObject();
            JArray array = new JArray();

            string id = "AA-0001-AA";
            

            json.Add("Cars", array);

            return Content(json.ToString());
        }


        // GET: api/Cars/5
        //[HttpGet("{id}", Name = "Get")]
        //public ActionResult Get(string id)
        //{
        //    if (!Parking.Instance.HasCars)
        //        return BadRequest(Helper.ErrorParkingEmpty);

        //    Vehicle car = Parking.Equals(id);

        //    if (car == null)
        //        return BadRequest(Helper.ErrorCarIdNotFound);

        //    return Content(Helper.CarToJObject(car, id).ToString());
        //}

        // POST: api/Cars/add
        //[HttpPost("add")]
        //public ActionResult Post()
        //{
        //    return AddCar(Vehicle.NewCar());
        //}

        //// POST: api/Cars/add/Bus;573
        //[HttpPost("add/{type};{balance}")]
        //public ActionResult Post(VehicleType type, decimal balance)
        //{
        //    switch (type)
        //    {
        //        case VehicleType.PassengerCar:
        //        case VehicleType.Truck:
        //        case VehicleType.Bus:
        //        case VehicleType.Motorcycle:
        //            if (balance >= Vehicle.MinAmount && balance <= Vehicle.MaxAmount)
        //                return AddCar(new Vehicle(type, balance));
        //            else
        //                return BadRequest(Helper.ErrorCarBalanceOutOfRange);
        //        default:
        //            return BadRequest(Helper.ErrorWrongCarType);
        //    }
        //}

        //// POST: api/Cars/addrange/10
        //[HttpPost("addrange/{count}")]
        //public ActionResult Post(int count)
        //{
        //    if (Parking.Instance.CountFreeParkingSpace == 0)
        //        return BadRequest(Helper.ErrorParkingFilled);

        //    int created = 0;
        //    JObject json = new JObject();
        //    JArray array = new JArray();

        //    for (int i = 0; i < count && Parking.Instance.CountFreeParkingSpace > 0; i++, created++)
        //    {
        //        try
        //        {
        //            Vehicle car = Vehicle.NewCar();                 
        //            array.Add(Helper.CarToJObject(car, id));
        //        }
        //        catch (InvalidOperationException)// parking filled
        //        {
        //            break;
        //        }
        //    }

        //    if (created == 0)
        //        return BadRequest("Помилка. Не вдалось створити ні однієї машини.");

        //    json.Add("Cars", array);

        //    return Content(json.ToString());
        //}

        // DELETE: api/cars/delete/5
        //[HttpDelete("delete/{id}")]
        //public ActionResult Delete(string id)
        //{
        //    if (!Parking.Instance.HasCars)
        //        return BadRequest(Helper.ErrorParkingEmpty);

        //    Vehicle car = Parking.Equals(id);

        //    if (car == null)
        //        return BadRequest(Helper.ErrorCarIdNotFound);
        //    else if (Parking.Instance.RemoveVehicle(car))
        //        return Content(Helper.CarToJObject(car, id).ToString());
        //    else if (car.Balance < 0)
        //        return BadRequest("Помилка. Машина оштрафована. Спочатку поповніть баланс.");
        //    else
        //        return BadRequest("Помилка. Машину не вдалось видалити. Причина невідома.");
        //}


        //private ActionResult AddCar(Vehicle car)
        //{
        //    try
        //    {
        //        if (Parking.Instance.CountFreeParkingSpace > 0)
        //        {
        //            string id = Parking.Instance.AddVehicle(car);

        //            return Content(Helper.CarToJObject(car, id).ToString());
        //        }
        //        else
        //        {
        //            return BadRequest(Helper.ErrorParkingFilled);
        //        }
        //    }
        //    catch (InvalidOperationException)
        //    {
        //        return BadRequest(Helper.ErrorParkingFilled);
        //    }
        //}
    }
}
