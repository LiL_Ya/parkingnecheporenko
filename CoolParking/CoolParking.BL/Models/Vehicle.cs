﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
       
        // TODO: implement class Vehicle.
        //       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
        //       The format of the identifier is explained in the description of the home task.
        //       Id and VehicleType should not be able for changing.
        //       The Balance should be able to change only in the CoolParking.BL project.
        //       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
        //       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

        public string Id { get; private set; }
        public decimal Balance { get; set; }
        
        public VehicleType vehicleType { get; private set; }


        public Vehicle() { }

        public Vehicle(string id,  VehicleType vehicleType, decimal balance)
        {
            this.Id = id;
            this.Balance = balance;            
            this.vehicleType = vehicleType;
        }
        public override string ToString()
        {
            return $"ID: {Id.ToString()} Баланс: {Balance.ToString()} Тип: {vehicleType.ToString()}";
        }
        public readonly static decimal MinAmount = 50;
        public readonly static decimal MaxAmount = 1000;

        private readonly static Random rnd = new Random();

        public static Vehicle NewCar() => new Vehicle("AA-0001-AA", VehicleType.Truck, 100);
    }
    }


