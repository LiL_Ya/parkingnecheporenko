﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using System.IO;

namespace CoolParking.BL.Models
{
    public sealed class Parking
    {
        #region Singleton

        private static readonly Lazy<Parking> _instance = new Lazy<Parking>(() => new Parking());

        public static Parking Instance => _instance.Value;

        #endregion

        #region Fields

       
        private readonly object LockObj = new object();
        private readonly string TransactionsLogFileName = "Transactions.log";
        private readonly TimeSpan OneMinute = TimeSpan.FromMinutes(1);


        private readonly List<Vehicle> _cars;
        private readonly List<TransactionInfo> _transactions;
        private readonly Timer _timerParkingPayment;
        private readonly Timer _timerTransactionsLog;
        private decimal _balance;

        #endregion

        #region Properties
      
        public decimal Balance
        {
            get => _balance;
            private set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("The balance cannot be less than zero.");

                _balance = value;
            }
        } 
    
        
        public int CountFreeParkingSpace => Settings.ParkingSpace - _cars.Count;
        
        public int CountOccupiedParkingSpace => _cars.Count;

        public bool HasCars
        {
            get
            {
                lock (LockObj) { return _cars.Count > 0; }
            }
        }

        #endregion

        #region Constructors

        private Parking()
        {
            _cars = new List<Vehicle>(Settings.ParkingSpace);
            _transactions = new List<TransactionInfo>();

            _timerParkingPayment = new Timer(Settings.Timeout * 1000);
            _timerParkingPayment.AutoReset = true;
            _timerParkingPayment.Elapsed += TimerParkingPayment_Elapsed;
            //_timerParkingPayment.Start();

            _timerTransactionsLog = new Timer(60 * 1000);
            _timerTransactionsLog.AutoReset = true;
            _timerTransactionsLog.Elapsed += TimerTransactionsLog_Elapsed;
            _timerTransactionsLog.Start();
        }

        #endregion

        #region Private Methods

        private void TimerParkingPayment_Elapsed(object sender, ElapsedEventArgs e)
        {
            lock (LockObj)
            {
                foreach (Vehicle car in _cars)
                {
                    decimal parkingPrice = Settings.GetParkingPrice(car.vehicleType);
                    

                    if (parkingPrice <= car.Balance)
                    {
                        car.Balance -= parkingPrice;
                        Balance += parkingPrice;                        
                        _transactions.Add(new TransactionInfo(DateTime.Now, car.Id, parkingPrice));
                    }
                    else
                    {                       
                        car.Balance -= parkingPrice * Settings.Fine;
                    }
                }
            }
        }

        private void TimerTransactionsLog_Elapsed(object sender, ElapsedEventArgs e)
        {
            lock (LockObj)
            {
                decimal debited = GetDebitedForLastMinute();

                try
                {
                    File.AppendAllText(TransactionsLogFileName,
                        $"[{DateTime.Now.ToString()}]: {debited.ToString()}\r\n",
                        Encoding.UTF8);
                }
                catch (DirectoryNotFoundException)
                { }
                catch (IOException)
                { }
                catch (UnauthorizedAccessException)
                { }
                
                ClearOldTransactions();
            }
        }

        private void ClearOldTransactions()
        {
            TimeSpan TwoMinutes = TimeSpan.FromMinutes(2);
            DateTime dtNow = DateTime.Now;
            int count = 0;

            foreach (var item in _transactions)
            {
                if (dtNow - item.DateTime > TwoMinutes)
                    count++;
                else
                    break;
            }

            if (count > 0)
                _transactions.RemoveRange(0, count);
        }

        #endregion

        #region Public Methods

     
        public void AddVehicle(Vehicle car)
        {
            lock (LockObj)
            {
                if (car == null)
                    throw new ArgumentNullException(nameof(car));

                if (CountFreeParkingSpace == 0)
                    throw new InvalidOperationException("There are no parking spaces available.");

                _cars.Add(car);

                if (_cars.Count == 1)
                    _timerParkingPayment.Start();
            }
        }

     
        public bool RemoveVehicle(string id)
        {
            lock (LockObj)
            {
                Vehicle car = GetCar(id);

                if (car == null)
                    return false;

                int index = _cars.IndexOf(car);

                if (index >= 0)
                {
                    if (car.Balance < 0)
                        return false;

                    _cars.RemoveAt(index);

                    if (_cars.Count == 0)
                        _timerParkingPayment.Stop();

                    return true;
                }

                return false;
            }
        }

   
        public bool TopUpVehicle(string id, decimal amount)
        {
            lock (LockObj)
            {
                if (amount <= 0)
                    throw new ArgumentOutOfRangeException(nameof(amount), "The top-up amount must be greater than zero.");

                Vehicle car = GetCar(id);

                if (car != null)
                    car.Balance += amount;

                return car != null;
            }
        }

        public Vehicle GetCar(string id) => _cars.Find(c => c.Id == id);

       
        public string GetLastParkingTransactions()
        {
            lock (LockObj)
            {
                DateTime dtNow = DateTime.Now;
                StringBuilder sb = new StringBuilder();
                string separator = new string('-', 50);

                sb.AppendLine(separator);
                sb.AppendLine($"|{"Время",-20}|{"ID машины",-10}|{"Списано средств",-16}|");
                sb.AppendLine(separator);

                int saveLength = sb.Length;

                foreach (var item in Enumerable.Reverse(_transactions))
                {
                    if (dtNow - item.DateTime > OneMinute)
                        break;

                    sb.AppendLine($"|{item.DateTime.ToString(),-20}|{item.CarId.ToString(),-10}|{Decimal.Round(item.Sum, 10).ToString(),-16}|");
                    sb.AppendLine(separator);
                }

                if (saveLength != sb.Length)
                    return sb.ToString();

                return String.Empty;
            }
        }


        public decimal GetDebitedForLastMinute()
        {
            lock (LockObj)
            {
                DateTime dtNow = DateTime.Now;
                decimal debited = Decimal.Zero;

                foreach (var item in Enumerable.Reverse(_transactions))
                {
                    if (dtNow - item.DateTime > OneMinute)
                        break;

                    debited += item.Sum;
                }

                return debited;
            }
        }

      
        public string ReadFromLog()
        {
            lock (LockObj)
            {
                try
                {
                    return File.ReadAllText(TransactionsLogFileName, Encoding.UTF8);
                }
                catch (DirectoryNotFoundException)
                {
                    return String.Empty;
                }
                catch (FileNotFoundException)
                {
                    return String.Empty;
                }
                catch (IOException)// не удалось прочитать файл
                {
                    return String.Empty;
                }
                catch (UnauthorizedAccessException)// отсутствует необходимое разрешение для чтения файла
                {
                    return String.Empty;
                }
            }
        }

             public string GetCarsInfo()
        {
            lock (LockObj)
            {
                StringBuilder sb = new StringBuilder();
                string separator = new string('-', 40);
                sb.AppendLine(separator);
                sb.AppendLine($"|{"ID",-10}|{"Баланс",-10}|{"Тип",-16}|");
                sb.AppendLine(separator);

                foreach (Vehicle car in _cars)
                {
                    sb.AppendLine($"|{car.Id.ToString(),-10}|{Decimal.Round(car.Balance, 10).ToString(),-10}|{car.vehicleType.ToString(),-16}|");
                    sb.AppendLine(separator);
                }

                return sb.ToString();
            }
        }

        public List<TransactionInfo> GetTransactionHistoryForLastMinute(Vehicle car)
        {
            lock (LockObj)
            {
                if (car == null)
                    return new List<TransactionInfo>();

                DateTime dtNow = DateTime.Now;

                return Enumerable.Reverse(_transactions)
                    .TakeWhile(t => dtNow - t.DateTime <= OneMinute)
                    .Where(t => t.CarId == car.Id)
                    .ToList();
            }
        }

        #endregion
    }
}
