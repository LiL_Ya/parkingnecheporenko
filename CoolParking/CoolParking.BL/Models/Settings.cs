﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        // TODO: implement class Settings.
        //       Implementation details are up to you, they just have to meet the requirements of the home task.

        
            private static readonly int _timeout = 5;
            private static readonly int _parkingSpace = 10;
            private static readonly decimal _fine = 2.5M;

            private static readonly Dictionary<VehicleType, decimal> _prices
                = new Dictionary<VehicleType, decimal>()
            {
            { VehicleType.Truck, 5 },
            { VehicleType.PassengerCar, 2 },
            { VehicleType.Bus, 3.5m },
            { VehicleType.Motorcycle, 1 }
            };

         
            public static int Timeout => _timeout;
         
            public static int ParkingSpace => _parkingSpace;
           
            public static decimal Fine => _fine;
           
            public static decimal GetParkingPrice(VehicleType carType)
            {
                if (_prices.TryGetValue(carType, out decimal price))
                    return price;

                throw new InvalidOperationException("No price is set for the specified machine type.");
            }

            static Settings()
            {

            }
    }
}    
