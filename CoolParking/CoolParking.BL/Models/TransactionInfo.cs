﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        // TODO: implement struct TransactionInfo.
        //       Necessarily implement the Sum property (decimal) - is used in tests.
        //       Other implementation details are up to you, they just have to meet the requirements of the homework.

        
        public decimal _debited;       
        public DateTime DateTime { get; private set; }        
        public string CarId { get; private set; }
       

        public decimal Sum { 
            get => _debited;
            private set
            {
                if (value< 0)
                    throw new ArgumentOutOfRangeException("The value of the funds written off cannot be less than zero.");

                _debited = value;
            }
}

        public TransactionInfo(DateTime dateTime, string carId, decimal debited)
        {
            
            DateTime = dateTime;
            CarId = carId;
            _debited= debited;
            
        }
      
        public override string ToString()
        {
            return $"Время: {DateTime.ToString()} ID машины: {CarId.ToString()} Списано средств: {_debited.ToString()}";
        }
    }
}
