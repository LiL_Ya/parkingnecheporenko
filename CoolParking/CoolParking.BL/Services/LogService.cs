﻿using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private readonly string _logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
        private readonly ILogService _logService;

        public LogService()
        {
            _logService = new LogService(_logFilePath);
        }

        public void Dispose()
        {
            File.Delete(_logFilePath);
        }
        public string logFilePath;

        public LogService(string logFilePath)
        {
            this.logFilePath = logFilePath;
        }

        // TODO: implement the LogService class from the ILogService interface.
        //       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
        //       Other implementation details are up to you, they just have to match the interface requirements
        //       and tests, for example, in LogServiceTests you can find the necessary constructor format.
        public string LogPath => throw new NotImplementedException();

        public string Read()
        {
            throw new InvalidOperationException();
            
        }

        public void Write(string logInfo)
        {
            throw new NotImplementedException();
        }
    }
}
