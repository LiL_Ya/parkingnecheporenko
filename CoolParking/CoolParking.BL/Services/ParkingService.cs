﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        readonly TimerService _withdrawTimer;
        readonly ParkingService _parkingService;
        readonly TimerService _logTimer;
        readonly ILogService _logService;

        public ParkingService(TimerService withdrawTimer, TimerService logTimer, ILogService logService)
        {
            this._withdrawTimer = withdrawTimer;
            this._logTimer = logTimer;
            this._logService = logService;
        }


        // TODO: implement the ParkingService class from the IParkingService interface.
        //       For try to add a vehicle on full parking InvalidOperationException should be thrown.
        //       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
        //       Other validation rules and constructor format went from tests.
        //       Other implementation details are up to you, they just have to match the interface requirements
        //       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
        public void AddVehicle(Vehicle vehicle)
        {
            throw new NotImplementedException();
        }

        public decimal Balance()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            _parkingService.Dispose();
        }


        public decimal GetBalance()
        {
            throw new NotImplementedException();
        }

        public int GetCapacity()
        {
            throw new NotImplementedException();
        }

        public int GetFreePlaces()
        {
            throw new NotImplementedException();
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            throw new NotImplementedException();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            throw new NotImplementedException();
        }

        public string ReadFromLog()
        {
            throw new NotImplementedException();
        }

        public void RemoveVehicle(string vehicleId)
        {
            throw new NotImplementedException();
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            throw new NotImplementedException();
        }
    }
}
